The excel file PaperH2_Pc-Kr _Data.xlsx stores all digital information related to the test cases of the article "Pore-scale modelling and sensitivity analyses of hydrogen-brine multiphase flow in geological porous media" by "Leila Hashemi, Martin J. Blunt, Hadi Hajibeygi".

We aim to release all pore-scale studies related to H2 storage in this address.

Stay connected; or contact Hadi Hajibeygi, to get notifications of the updates.

Professor Hadi Hajibeygi, 21 October 2020, Delft.
